package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"

	"github.com/pkg/errors"
)

func main() {

	logFile := "state.log"

	// Init
	var option string

	if len(os.Args) > 1 {
		option = os.Args[1]
	} else {
		if fileExists(logFile) {
			option = "check"
		} else {
			option = "backup"
		}
	}

	if option == "backup" {
		fmt.Println("Saving running services...")
		err := logRunningServices(logFile)
		errorHandler(err)

		fmt.Println("Saving mountpoints...")
		err = logMountPoints(logFile)
		errorHandler(err)

		fmt.Println("Saving network interfaces...")
		err = logInterfaces(logFile)
		errorHandler(err)

	} else if option == "check" {

		// Read log file
		lines, err := readLines(logFile)
		errorHandler(err)

		// Get data from log file
		data := getDataBU(lines)
		services := data[0]
		mountPoints := data[1]
		interfaces := data[2]

		fmt.Println("\nChecking services...")
		checkServices(services)

		fmt.Println("\nChecking mountpoints...")
		err = checkMountPoints(mountPoints)
		errorHandler(err)

		fmt.Println("\nChecking network interfaces...")
		err = checkInterfaces(interfaces)
		errorHandler(err)

	} else if option == "clean" {
		fmt.Println("Cleaning...")
		err := os.Remove(logFile)
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Println("DONE!")
		}

	} else if option == "help" {
		fmt.Printf("\nUsage:\n%s backup\n%s check\n%s clean\n%s help\n\n",
			os.Args[0],
			os.Args[0],
			os.Args[0],
			os.Args[0])
		os.Exit(0)

	} else {
		fmt.Printf("error: option %s not found\n", option)
		fmt.Printf("\nUsage:\n%s backup\n%s check\n%s clean\n%s help\n\n",
			os.Args[0],
			os.Args[0],
			os.Args[0],
			os.Args[0])
		os.Exit(2)
	}

	fmt.Println("\nDONE!")

}

// errorHandler exists with error code if err is not nil
func errorHandler(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// getComand checks if systemctl is present and returns service if not
func getCommand() (string, error) {
	path, _ := exec.LookPath("systemctl")
	if path == "" {
		path, err := exec.LookPath("service")
		if path == "" {
			return "", err
		}
		return path, nil
	}
	return path, nil
}

// Check for linux commmand in the system
func commandExists(command string) bool {
	path, _ := exec.LookPath(command)
	return path != ""
}

// isActive returns true if state is active, otherwise returns false
func isActive(service string) bool {
	cmd := exec.Command("systemctl", "check", service)
	out, err := cmd.CombinedOutput()
	if err != nil {
		return false
	}
	outs := strings.TrimSpace(string(out))
	if outs != "active" {
		fmt.Println(outs)
		return false
	}

	return true
}

// writeLog gets the output of a systemctl list-units command, writes the
// services into a file and returns the file handler
func writeLog(out []byte, logFile string) (*os.File, error) {

	// Split output by lines
	s := strings.Split(string(out), "\n\n")[0]
	lines := strings.Split(s, "\n")

	// Get just service names from whole line
	var ll []string
	for k, line := range lines {
		if k > 0 {
			ss := strings.TrimSpace(line)
			ss = strings.Split(ss, " ")[0]
			ss = strings.Split(ss, ".")[0]
			ll = append(ll, ss)
		}
	}

	// Remove file if exists
	if fileExists(logFile) {
		os.Remove(logFile)
	}

	// Open/create file to write log in
	f, err := os.OpenFile(logFile, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return nil, err
	}

	// Write services names to file
	f.WriteString("[services]\n")
	for _, v := range ll {
		f.WriteString(v + "\n")
	}

	return f, nil
}

// fileExists returns true if filepath file exists, otherwise returns false
func fileExists(filepath string) bool {

	// Get file info
	fileinfo, err := os.Stat(filepath)
	if os.IsNotExist(err) {
		return false
	}

	// Return false if the fileinfo says the file path is a directory.
	return !fileinfo.IsDir()
}

// readLines returns lines within given file as strings array
func readLines(file string) ([]string, error) {

	var lines []string

	f, err := os.Open(file)
	if err != nil {
		return lines, err
	}
	defer f.Close()

	s := bufio.NewScanner(f)
	for s.Scan() {
		lines = append(lines, s.Text())
	}
	if err := s.Err(); err != nil {
		return lines, err
	}

	return lines, nil
}

// checkServices checks services in given array with systemctl and prompts the results
func checkServices(services []string) {
	for _, service := range services {
		if isActive(service) {
			fmt.Printf("[OK]  %s\n", service)
		} else {
			fmt.Printf("[KO]! %s\n", service)
		}
	}
}

// getMountPoints returns mountpoints found through df command
func getMountPoints() (mp []string) {

	// Use df command to get mountpoints
	cmd := exec.Command("df")
	out, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println(err)
	}

	// Format output to return just mountpoints
	lines := strings.Split(string(out), "\n")
	for i, line := range lines {
		if i == 1 {
			continue
		}
		e := strings.Fields(line)
		if len(e) > 5 {
			fline := fmt.Sprintf("%s %s", e[0], e[5])
			mp = append(mp, fline)
		}
	}

	return
}

// getInterfaces returns IP and network interfaces found with ip addr command
func getInterfaces() ([]string, error) {
	var ifs []string
	cmd := exec.Command("ip", "-oneline", "addr", "show")
	out, err := cmd.CombinedOutput()
	if err != nil {
		return ifs, err
	}

	lines := strings.Split(string(out), "\n")
	for i, line := range lines {
		l := strings.Fields(line)
		if len(l) > 3 && i%2 == 0 {
			ifs = append(ifs, l[1]+" "+l[3])
		}
	}

	return ifs, nil
}

// logRunningServices writes running services found with systemctl to log file
func logRunningServices(logFile string) error {
	if commandExists("systemctl") {

		// Build and run command
		cmd := exec.Command("systemctl", "list-units", "--state=running", "--type=service")
		out, err := cmd.CombinedOutput()
		if err != nil {
			return err
		}

		// Write running services to file
		_, err = writeLog(out, logFile)
		if err != nil {
			return err
		}
		// fmt.Println("Running services written to " + logFile)

	} else {
		return errors.New("systemctl not found")
	}

	return nil
}

// logMountPoints writes mount points found with df command to log file
func logMountPoints(logFile string) error {

	// Get mountpoints
	mp := getMountPoints()

	f, err := os.OpenFile(logFile, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}

	f.WriteString("\n[mountpoints]\n")
	for _, v := range mp {
		f.WriteString(v + "\n")

	}

	return nil
}

// logMountPoints writes IP and network interfaces to log file
func logInterfaces(logFile string) error {

	// Get interfaces
	ifs, err := getInterfaces()
	if err != nil {
		return err
	}

	f, err := os.OpenFile(logFile, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}

	f.WriteString("\n[interfaces]\n")
	for _, v := range ifs {
		f.WriteString(v + "\n")
	}

	return nil
}

// getDataBU extracts blocks from log file
func getDataBU(lines []string) (block [][]string) {

	var k int
	for i, line := range lines {
		if line == "[services]" {
			k = i
		}
		if line == "[mountpoints]" {
			block = append(block, lines[k+1:i-1])
			k = i
		}
		if line == "[interfaces]" {
			block = append(block, lines[k+1:i-1])
			block = append(block, lines[i+1:])
		}
	}

	return block
}

// checkMountPoints checks if given mountpoints are currently mounted
func checkMountPoints(mpBU []string) error {
	mp := getMountPoints()

	for _, v := range mpBU {
		if contains(mp, v) {
			v = strings.Replace(v, " ", " on ", 1)
			fmt.Printf("[OK]  %s\n", v)
		} else {
			v = strings.Replace(v, " ", " on ", 1)
			fmt.Printf("[KO]! %s\n", v)
		}
	}

	return nil
}

// checkInterfaces checks if given IP and interfaces are currently up
func checkInterfaces(ifsBU []string) error {
	ifs, err := getInterfaces()
	if err != nil {
		return err
	}
	for _, v := range ifsBU {
		if contains(ifs, v) {
			fmt.Printf("[OK]  %s\n", v)
		} else {
			fmt.Printf("[KO]! %s\n", v)
		}
	}

	return nil
}

// contains returns true if given str is within s, false otherwise
func contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}
